<?php
/**
 * @file
 * Administrative page callbacks for the Comment Country module.
 */
  
/**
 * Comment_country administration settings.
 *
 * @return
 *   Form for comment_country configuration options.
 */
function comment_country_settings() {
  $form['comment_country_admin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select what country info to display in comments:'),
  );
  $form['comment_country_admin']['comment_country_name'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display country name in comments.'),
    '#return_value' => '1',
    '#default_value' => variable_get('comment_country_name', 1),
  );
  $form['comment_country_admin']['comment_country_code'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display country code in comments.'),
    '#return_value' => '1',
    '#default_value' => variable_get('comment_country_code', 0),
  );
  $form['comment_country_admin']['comment_country_flag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display country flag in comments.'),
    '#return_value' => '1',
    '#default_value' => variable_get('comment_country_flag', 0),
    '#disabled' => !module_exists('countryicons'),
    '#description' => module_exists('countryicons') ? t('The Country Icons module is installed and enabled.') : '<em><span style="color: #A50000;">'. t('You must install the ') . l('Country Icons', 'http://drupal.org/project/countryicons') . t(' module to use this feature.') . '</span></em>'
  );
  $form['comment_country_update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update comment country info:'),
    '#description' => '<span style="color: red; font-weight: bold;">' . t('WARNING: this will delete all current data. Use with care.') . "</span>",
  );
  $form['comment_country_update']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Comment Country Info'),
  );
  $form['#submit'][] = 'comment_country_admin_submit';
  return system_settings_form($form);
}

/**
* admin settings form handler
*
* Use the additional button to update all comment country info.
*/
function comment_country_admin_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Update Comment Country Info') {
    db_query("TRUNCATE TABLE {comment_country}");
    $result = db_query("SELECT {cid, hostname} FROM {comments} WHERE {status = 0}");
  
    $count = 0;
    while ($data = db_fetch_object($result)) {
      $xml = simplexml_load_string(drupal_http_request('http://ipinfodb.com/ip_query.php?ip=' . $data->hostname)->data);
      db_query("INSERT INTO {comment_country} (cid, country_code, country_name) VALUES (%d, '%s', '%s')", $data->cid, $xml->CountryCode, $xml->CountryName);
      $count++;
    };
  
    if ($count) {
      drupal_set_message(t('The country information for '. $count . ' comments has been updated successfully.'));
      return;
    }
  
    drupal_set_message(t('Could not update comment country information.'));
    return FALSE;
  }
}