
The Comment Country module does one small thing but does it well. It enables the
display of the author's country information in comments. Available formats are
the country name, the 2 letter country code, and the country flag(which requires
the http://drupal.org/project/countryicons module).
  
Since the module uses the api available at http://ipinfodb.com, it does not 
require the installation and maintainence of any type of country database. It 
looks up and stores the comment author's country information upon comment creation
and update. There's also an option on on the configuration settings page to refresh
all comment country info at once.
  
NOTE 1: NOTHING will display by default. You must edit your theme to print the 
appropriate $comment properties in your theme's comment.tpl.php file.
  
NOTE 2: For convenience, if the countryicons module is enabled, the comment_country
module uses the theme_countryicons_icon_sprite theme function (for performance) to
display the country flag as a css sprite. You can always disable the flag display
option in the comment_country settings and generate the flag display manually using
whatever countryicons method you prefer. See the country icons module documentation
for more information.
  
  
INSTALLATION
------------
  
Install the the module like any other drupal module. 
See http://drupal.org/getting-started/install-contrib/modules for more information.
  
  
USAGE
-----
  
The module adds 1, 2, or 3 properties (depending upon the configuration settings)
to the $comment object for use in the comment.tpl.php file of your theme.
  
1. select the desired display options at admin/configuration/comment_country.
 
2. edit the comment.tpl.php file for your theme. If your theme doesn't have one,
   simply copy the core file from modules/comment/comment.tpl.php to your theme's
   subdidectory.
 
3. add any or all of the following statements to print the info as desired:
     To print the country name in the comment use:
        <?php print $comment->comment_country_name; ?>
         
     To print the country code in the comment use:
        <?php print $comment->comment_country_code; ?>
      
     To print the country flag in the comment use:
        <?php print $comment->comment_country_flag; ?>


TROUBLESHOOTING
---------------

NOTE: Due to the use of simplexml, this module requires PHP5. Please verify your 
version of php and don't file bug reports or support requests for problems on PHP4.

Before creating an issue in the module's issue queue, please verify that 1) you've
enabled the proper display options at admin/configuration/comment_country AND 2) 
added the appropriate print statement(s) to your theme's commment.tpl.php. 99.99% 
of the time the problem is that one or the other of those two things have not been
done.
  